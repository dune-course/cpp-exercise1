#include <iomanip>
#include <iostream>
#include <vector>

/**
 * Matrix Class representing matrices of double values
 */

class MatrixClass
{
 public:
  
  // Set number of matrix rows and columns and initialize its elements with value
  void Resize(int numRows, int numCols, const double &value = double() );
  
  // Access matrix element at position (i,j)
  double &operator()(int i, int j);
  double operator()(int i, int j) const;
  
  // Arithmetic functions 
  MatrixClass &operator*=(double x);
  MatrixClass &operator+=(const MatrixClass &b);
  
  // Output matrix content
  void Print() const;
  
  // Returns number of matrix raws
  int Rows() const
    {
      return numRows_;
    }
  
  // Returns number of matrix columns
  int Cols() const
    {
      return numCols_;
    }
  
  
  // Constructors
  MatrixClass(int numRows=0, int numCols=0, const double &value=double() ) :
    a_(numRows), numRows_(numRows), numCols_(numCols)
    {
      if ( ((numCols==0) && (numRows!=0)) || ((numCols!=0) && (numRows==0)) )
	{
	  numRows_=0;
	  numCols_=0;
	  a_.resize(numRows_);
	}
      else
	{
	  for (int i=0;i<numRows_;++i)
	    a_[i].resize(numCols_);
	  numRows_=numRows;
	  numCols_=numCols;	    
	}
      for (int i=0;i<numRows;++i)
	{
	  for (int j=0;j<numCols;++j)
	    a_[i][j]=value;
	}
    }
  
 private:
  // matrix elements
  std::vector<std::vector<double> > a_;
  // number of rows
  int numRows_;
  // number of columns
  int numCols_;
};

// More arithmetic functions
MatrixClass operator*(const MatrixClass &a, double x);
MatrixClass operator*(double x, const MatrixClass &a);
MatrixClass operator+(const MatrixClass &a, const MatrixClass &b);
