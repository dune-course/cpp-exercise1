\documentclass[a4paper,10pt,twoside]{scrartcl}

\typearea{12}
%\setcounter{page}{0}

%% beamer magic
\newcommand{\amode}[2]{#1}
\newcommand{\pmode}[2]{#2}

\usepackage{beamerarticle}
\usepackage{hyperref}
\setjobnamebeamerversion{lectures-beamer}
\parindent0pt
\parskip1ex

\usepackage{scrpage2}
\pagestyle{scrheadings}

\lehead{DUNE Course, Heidelberg \the\year}
\cehead{}
\rehead{\headmark}
\lohead{\headmark}
\cohead{}
\rohead{}
\automark[section]{part}

\usepackage{etex}

\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{url}
\usepackage{mathe}
\usepackage{graphicx}
\usepackage{calc}
\usepackage{multicol}
\usepackage{tikz}
%\usepackage{natbib}
\usepackage[english]{babel}

\usepackage{multibib}

\usepackage{animate}
\usepackage{animtikz}

% PDELAB Howto includes
  \usepackage{amscd}
  \usepackage{multimedia}
\usetikzlibrary{shapes,arrows}
\usepackage{curves}
\usepackage{listings}
\usepackage{picinpar}
\usepackage{enumerate}
%\usepackage{algorithmic}
%\usepackage{algorithm}
\usepackage{bm}
\usepackage{nicefrac}

\usepackage{hyperref}
  \setkomafont{pagenumber}{\normalfont\scshape}
  \setkomafont{pagehead}{\normalfont\scshape}

% \lstset{language=C++, basicstyle=\small\ttfamily,
%   keywordstyle=\bfseries, tabsize=4, stringstyle=\ttfamily,
%   commentstyle=\it, extendedchars=true, escapeinside={/*@}{@*/}}

\lstset{language=C++, basicstyle=\small\ttfamily,
 tabsize=4, stringstyle=\ttfamily,
 keywordstyle=\color{violet},
 commentstyle=\color{darkgray},
 stringstyle=\color{orange},
 emph={bool,int,unsigned,char,true,false,void}, emphstyle=\color{blue},
 emph={[2]\#include,\#define,\#ifdef,\#endif}, emphstyle={[2]\color{violet}},
 emph={[3]Dune,Grid,GridView,LeafGridView,LevelGridView,SomeGrid,TheGrid,LeafIterator,Iterator,LevelIterator,LeafIntersectionIterator,LevelIntersectionIterator,IntersectionIterator,LeafMultipleCodimMultipleGeomTypeMapper,Geometry,Entity,EntityPointer,Codim,FieldVector,FieldMatrix}, emphstyle={[3]\color{blue}},
extendedchars=true, escapeinside={/*@}{@*/}}


%The theorems
\mode<article>
{
\newtheoremstyle{mystyle}%
{3pt}%
{3pt}%
{}%
{}%
{\sffamily\bfseries}%
{.}%
{.5em}%
{}%
\theoremstyle{mystyle}
}

\theoremstyle{definition}

\newtheorem{Def}{Definition}%[section]
\newtheorem{Exm}[Def]{Example}
\newtheorem{Lem}[Def]{Lemma}
\newtheorem{Rem}[Def]{Remark}
\newtheorem{Rul}[Def]{Rule}
\newtheorem{Thm}[Def]{Theorem}
\newtheorem{Cor}[Def]{Corollary}
\newtheorem{Obs}[Def]{Observation}
\newtheorem{Ass}[Def]{Assumption}
\newtheorem{Pro}[Def]{Property}
\newtheorem{Alg}[Def]{Algorithm}
\newtheorem{Prp}[Def]{Proposition}
\newtheorem{Lst}[Def]{Listing}
% end

%% math symbols
\newcommand{\C}{\mathbb{C}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\K}{\mathbb{K}}
\newcommand{\blue}{\color[rgb]{0,0,1}}
\newcommand{\green}{\color[rgb]{0,1,0}}
\newcommand{\red}{\color[rgb]{1,0,0}}
\newcommand{\violet}{\color[rgb]{1,0,1}}

\renewcommand{\uu}{\mathbf{u}}
\renewcommand{\nn}{\mathbf{n}}

\newlength{\tmplen}

\def\clap#1{\hbox to 0pt{\hss#1\hss}}
\definecolor{dunedarkblue}{rgb}{0.22, 0.29, 0.49}
\definecolor{dunemediumblue}{rgb}{0.35, 0.45, 0.71}
\definecolor{dunelightblue}{rgb}{0.61, 0.68, 0.85}
\definecolor{duneorange}{rgb}{0.91, 0.56, 0.25}

%% some math commands
    \DeclareMathOperator{\dist}{dist}
    \newcommand{\lt}{\left}
    \newcommand{\rt}{\right}
    \newcommand{\cl}[1]{\overline{#1}}
    \newcommand{\Oi}{\Omega_i}
    \newcommand{\Oj}{\Omega_j}
    \newcommand{\hOi}{\hat\Omega_i}
    \newcommand{\hOj}{\hat\Omega_j}
    \newcommand{\cOi}{\overline{\Omega_i}}
    \newcommand{\cOj}{\overline{\Omega_j}}

% number equations within sections in article mode
%\numberwithin{equation}{section}

% math symbols
\newcommand{\diffd}{\,d}


% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:
% \AtBeginSection[]
% {
%   \begin{frame}<beamer>
%     \frametitle{Outline}
%     \tableofcontents[currentsection]
%   \end{frame}
% }

\begin{document}

%% ------------------------------------ BEGIN C++ exercises ---------------------------
\section*{C++ Exercise 1 - Templates}
\subsection*{Introduction}

\begin{onlyenv}<article>
As presented in the lecture, templates are a common generic
programming technique. They allow the design of algorithms independent of
a specific datatype.\\

In these exercises:
\begin{itemize}
  \item Template classes and functions
  \item Inheritance from template classes
\end{itemize}
\end{onlyenv}

\begin{frame}[fragile]
\frametitle{Templates}
Matrices are a central data structure in numerics and therefore used frequently.
Suppose we have the following situation:
\begin{itemize}
  \item We already have an implementation of a class \lstinline!MatrixClass!
representing 2D matrices of elements of type \lstinline!double!.
  \item In our applications we often want to have matrices of other
types of elements (\lstinline!int!, \lstinline!complex!, \ldots).
 \begin{itemize}
  \item One possibility is to implement a  \lstinline!MatrixClass! for each type of
element. This results in a lot of (redundant) code that is not easily
maintainable.
  \item We therefore want to create a template class \lstinline!MatrixClass!\\
($\rightarrow$ better code reuse, easier to maintain).
\end{itemize}
\end{itemize}


 \begin{block}{}
In this exercise you will change a given implementation of \lstinline!MatrixClass!
for elements of type \lstinline!double! into a template
class \lstinline!MatrixClass! to represents matrices of any type of elements
(\lstinline!double!, \lstinline!int!, \ldots).
\end{block}
\end{frame}
%-----------------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{MatrixClass for double elements :  Basic features}
\begin{itemize}
\item Matrix elements stored in a vector of vectors\\
          \lstinline!vector<vector<double> > a_;!
\item Methods to access the matrix elements
  \begin{itemize}
    \item  \lstinline!double &operator()(int i, int j);!
    \item  \lstinline!double operator()(int i, int j) const;!
  \end{itemize}
\item Arithmetic operations on matrices:
  \begin{itemize}
    \item Multiplication by a value\\
              \lstinline!MatrixClass &operator*=(double x);!
    \item Addition with another matrix
              \lstinline!MatrixClass &operator+=(const MatrixClass &b);!
  \end{itemize}
\end{itemize}
\vspace*{5pt}
More (non-class) operators for matrices that internally use the
arithmetic operations \lstinline!*=! and  \lstinline!+=!:
\begin{itemize}
  \item  \lstinline!operator*(const MatrixClass &a, double x);!
  \item \lstinline!operator*(double x,const MatrixClass &a);!
  \item \lstinline!operator+(const MatrixClass &a, const MatrixClass &b);!
\end{itemize}
\end{frame}
%-----------------------------------------------------------------------------

\clearpage
\begin{frame}[fragile]
\frametitle{MatrixClass (matrix\_double.h)}
\lstinputlisting[basicstyle=\scriptsize\ttfamily]
{../matrix_double/matrix_double.h}
\end{frame}

%-----------------------------------------------------------------------------

\clearpage
\begin{frame}[fragile]
\frametitle{Test the implementation (test\_matrix\_double.cc)}
\lstinputlisting[basicstyle=\scriptsize\ttfamily]
{../matrix_double/test_matrix_double.cc}
\end{frame}

%-----------------------------------------------------------------------------
\subsection*{Part I -- Template classes and functions}

\begin{frame}[fragile]
% \frametitle{Part I}
\begin{block}{}
Change the given implementation of the \lstinline!MatrixClass! for elements of
type \lstinline!double! into a template matrix class that allows handling of
different types of elements. Adapt the main routine to test your
implementation.\\
Note: The non-class functions
\begin{itemize}
  \item \lstinline!operator*(const MatrixClass &a, double x)!
  \item \lstinline!operator*(double x,const MatrixClass &a)! and
  \item \lstinline!operator+(const MatrixClass &a, const MatrixClass &b)!
\end{itemize}
have to be changed into template functions.

\emph{Hint}: If you encounter problems linking your application, you probably implemented the
templated matrix using the common technique of separating header (interface) and source (implementation) files.
That is not possible using templates! Have a look at this Stackoverflow question for details: \href{http://stackoverflow.com/q/495021/2429404}{http://stackoverflow.com/q/495021/2429404}.
The short version is: put everything in the header file!
\end{block}
\end{frame}

%-----------------------------------------------------------------------------
\subsection*{Part II -- Inheritance from template classes}

\begin{frame}[fragile]
  % \frametitle{Exercise 1 - Part II}
  \begin{block}{}
    Split up the templated \lstinline!MatrixClass! into two classes:\\
    \begin{itemize}
       \item A non-numeric matrix class \lstinline!MatrixClass! representing a matrix of
    elements (of different type) in general that provides:
    \begin{itemize}
  \item Constructors (as in \lstinline!MatrixClass!)
  \item \lstinline!operator()(int i, int j)! to access the matrix elements
  \item Resize and print- methods (as in  \lstinline!MatrixClass!)
    \end{itemize}
    \item A numeric matrix class \lstinline!NumMatrixClass! that derives from
    \lstinline!MatrixClass! representing a numerical matrix allowing the arithmetic
    operations on matrices.
\end{itemize}
    Adapt the non-class functions and the main routine and test your implementation.
  \end{block}
\end{frame}


%% ------------------------------------ END C++ exercises ---------------------------

\end{document}
